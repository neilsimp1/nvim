local map_opts = { silent = false }
function KeyMap(mode, lhs, rhs, desc, nowait, remap)
	nowait = nowait or false
	map_opts.desc = desc
	map_opts.nowait = nowait
	if remap then map_opts.remap = true end
	vim.keymap.set(mode, lhs, rhs, map_opts)
end

NeoTreeSource = "filesystem"
NeoTreeSources = {
	"filesystem",
	"buffers",
	"git_status",
	"document_symbols",
}

TroubleMode = "diagnostics"
TroubleModes = {
	"diagnostics",
	"lsp_references",
	"lsp_definitions",
	"lsp_type_definitions",
	"quickfix",
	"loclist",
}
