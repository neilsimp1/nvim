return {
	"tpope/vim-fugitive",

	{
		"FabijanZulj/blame.nvim",
		config = function()
			require("blame").setup({
				date_format = "%Y-%m-%d %H:%M",
				merge_consecutive = false,
				virtual_style = "float",
			})

			KeyMap("n", "<leader>sb", "<Cmd>BlameToggle virtual<CR>")
		end,
	},

	{
		"lewis6991/gitsigns.nvim",
		config = function()
			require("gitsigns").setup()
		end,
	},

	{
		"kdheepak/lazygit.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		config = function()
			KeyMap("n", "<leader>sc", "<Cmd>LazyGit<CR>")
			KeyMap("n", "<leader>sf", "<Cmd>LazyGitFilterCurrentFile<CR>")
		end,
	},
}
