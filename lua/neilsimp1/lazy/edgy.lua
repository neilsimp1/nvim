local left_size = { width = 42 }

return {
	"folke/edgy.nvim",
	event = "VeryLazy",
	opts = {
		animate = { enabled = false },
		left = {
			{
				ft = "neo-tree",
				title = function()
					return vim.fn.fnamemodify(vim.loop.cwd(), ":~")
				end,
				size = left_size,
			},
			{
				ft = "dbui",
				title = "DB Connections",
				size = left_size,
			},
			{
				ft = "DiffviewFiles",
				title = "Source Control",
				size = left_size,
			},
			{
				ft = "undotree",
				title = "Undotree",
				size = left_size,
			},
		},
		bottom = {
			{
				ft = "trouble",
				title = function()
					return "Trouble / " .. TroubleMode
				end,
			},
			{
				ft = "DiffviewFileHistory",
				title = "File History",
			},
			{
				ft = "diff",
				title = "Undotree diff"
			},
		},
	},
}
