return {

	"famiu/bufdelete.nvim",

	{
		"akinsho/bufferline.nvim",
		version = "*",
		dependencies = "nvim-tree/nvim-web-devicons",
		config = function()
			local get_file_explorer_text = function(default)
				local text = default
				local branch = vim.fn.system("git branch --show-current 2> /dev/null | tr -d '\n'")
				if branch ~= nil and branch ~= "" then
					text = vim.fn.substitute(vim.fn.getcwd(), "^.*/", "", "") .. " / " .. branch
				end
				return text
			end

			require("bufferline").setup({
				options = {
					diagnostics = "nvim_lsp",
					close_command = "Bdelete %d",
					middle_mouse_command = "Bdelete %d",
					right_mouse_command = "vsplit",
					offsets = {
						{
							filetype = "neo-tree",
							text = get_file_explorer_text,
							text_align = "center",
							separator = false,
							highlight = "BufferLineFill",
						},
						{ filetype = "dapui_breakpoints" },
						{ filetype = "dapui_scopes" },
						{ filetype = "dapui_stacks" },
						{ filetype = "dapui_watches" },
						{ filetype = "dbui" },
						{
							filetype = "DiffviewFiles",
							text = get_file_explorer_text,
							text_align = "center",
							separator = false,
							highlight = "BufferLineFill",
						},
					},
					separator_style = "slant",
				},
			})

			-- Re-order to previous/next
			KeyMap("n", "<C-,>", "<Cmd>BufferLineMovePrev<CR>") -- kitty.conf is mapping `<C-[>` to `<C-,>`
			KeyMap("n", "<C-]>", "<Cmd>BufferLineMoveNext<CR>")

			-- Goto buffer in position...
			KeyMap("n", "<C-1>", "<Cmd>BufferLineGoToBuffer 1<CR>")
			KeyMap("n", "<C-2>", "<Cmd>BufferLineGoToBuffer 2<CR>")
			KeyMap("n", "<C-3>", "<Cmd>BufferLineGoToBuffer 3<CR>")
			KeyMap("n", "<C-4>", "<Cmd>BufferLineGoToBuffer 4<CR>")
			KeyMap("n", "<C-5>", "<Cmd>BufferLineGoToBuffer 5<CR>")
			KeyMap("n", "<C-6>", "<Cmd>BufferLineGoToBuffer 6<CR>")
			KeyMap("n", "<C-7>", "<Cmd>BufferLineGoToBuffer 7<CR>")
			KeyMap("n", "<C-8>", "<Cmd>BufferLineGoToBuffer 8<CR>")
			KeyMap("n", "<C-9>", "<Cmd>BufferLineGoToBuffer 9<CR>")
			KeyMap("n", "<C-0>", "<Cmd>BufferLineGoToBuffer 10<CR>")
			KeyMap("n", "<C-`>", "<Cmd>BufferLineGoToBuffer -1<CR>")

			-- Pin/unpin buffer
			KeyMap("n", "<C-p>", "<Cmd>BufferLineTogglePin<CR>")

			-- Close buffers
			KeyMap("n", "<C-w>", "<Cmd>Bdelete<CR>", "Close tab", true)
			KeyMap("n", "<C-S-w>", "<Cmd>BufferLineCloseOthers<CR>", "Close other tabs", true)
			KeyMap("n", "<C-S-[>", "<Cmd>BufferLineCloseLeft<CR>", "Close tabs to the left", true)
			KeyMap("n", "<C-S-]>", "<Cmd>BufferLineCloseRight<CR>", "Close tabs to the right", true)
		end,
	},
}
