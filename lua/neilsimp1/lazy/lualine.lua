return {
	"nvim-lualine/lualine.nvim",
	config = function()
		local noice_api = require("noice").api
		require("lualine").setup({
			options = {
				component_separators = "|",
				disabled_filetypes = { "neo-tree", "Trouble" },
				icons_enabled = false,
				section_separators = "",
				theme = "onedark",
			},
			sections = {
				lualine_x = {
					{
						noice_api.statusline.mode.get,
						cond = noice_api.statusline.mode.has,
						color = { fg = "#ff9e64" },
					}
				},
			},
		})
	end,
}
