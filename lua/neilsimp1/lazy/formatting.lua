return {
	"tpope/vim-sleuth",

	{
		"brenton-leighton/multiple-cursors.nvim",
		opts = {},
		keys = {
			{"<S-A-j>", "<Cmd>MultipleCursorsAddDown<CR>", mode = {"n", "x"}, desc = "Add cursor and move down"},
			{"<S-A-k>", "<Cmd>MultipleCursorsAddUp<CR>", mode = {"n", "x"}, desc = "Add cursor and move up"},
			{"<S-A-Up>", "<Cmd>MultipleCursorsAddUp<CR>", mode = {"n", "i", "x"}, desc = "Add cursor and move up"},
			{"<S-A-Down>", "<Cmd>MultipleCursorsAddDown<CR>", mode = {"n", "i", "x"}, desc = "Add cursor and move down"},
			{"<S-A-LeftMouse>", "<Cmd>MultipleCursorsMouseAddDelete<CR>", mode = {"n", "i"}, desc = "Add or remove cursor"},
		},
	},

	{
		"gregorias/coerce.nvim",
		config = function()
			require("coerce").setup({
				default_mode_keymap_prefixes = {
					visual_mode = "<leader><leader>c",
				},
			})
		end,
	},

	{
		"lukas-reineke/indent-blankline.nvim",
		main = "ibl",
		opts = {},
	},

	{
		"tzachar/local-highlight.nvim",
		opts = {},
	},

	{
		"Kicamon/markdown-table-mode.nvim",
		opts = {},
	},

	{
		"echasnovski/mini.ai",
		opts = {},
	},
}
