return {
	"coffebar/neovim-project",
	opts = {
		projects = {
			"~/projects/*",
			"~/projects/_aur/*",
			"~/projects/_external/*",
		},
		last_session_on_startup = false,
	},
	init = function()
		vim.opt.sessionoptions:append("globals")

		local function project_discover() vim.cmd("Telescope neovim-project discover") end
		local function project_history() vim.cmd("Telescope neovim-project history") end

		vim.api.nvim_create_autocmd({"UIEnter"}, {
			callback = function()
				if vim.fn.argc() == 0 then
					project_history()
				end
			end
		})

		-- Auto open neo-tree upon opening session
		vim.api.nvim_create_autocmd({"User"}, {
			pattern = "SessionLoadPost",
			callback = vim.schedule_wrap(function()
				vim.cmd("Neotree show")
			end),
		})

		KeyMap("n", "<C-S-o>", project_discover)
		KeyMap("n", "<C-S-r>", project_history)
	end,
	dependencies = {
		{ "nvim-lua/plenary.nvim" },
		{ "nvim-telescope/telescope.nvim", tag = "0.1.4" },
		{ "Shatur/neovim-session-manager" },
	},
	lazy = false,
	priority = 100,
}
