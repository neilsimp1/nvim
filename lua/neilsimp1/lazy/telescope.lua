return {
	"nvim-telescope/telescope.nvim",
	branch = "0.1.x",
	dependencies = {
		"nvim-lua/plenary.nvim",
		{
			"nvim-telescope/telescope-fzf-native.nvim",
			build = "make",
			cond = function()
				return vim.fn.executable "make" == 1
			end,
		},
	},
	config = function()
		local telescope = require("telescope")
		telescope.setup({
			defaults = {
				mappings = {
					i = {
						["<C-u>"] = false,
						["<C-d>"] = false,
					},
				},
			},
		})

		pcall(telescope.load_extension, "fzf")

		local builtin = require("telescope.builtin")
		KeyMap("n", "<leader>?", builtin.oldfiles, "Find recently opened files")
		KeyMap("n", "<leader>f", builtin.find_files, "Find files")
		KeyMap("n", "<leader>c", builtin.live_grep, "Find code")
		KeyMap("n", "<leader>/", function()
			builtin.current_buffer_fuzzy_find(require("telescope.themes").get_dropdown {
				winblend = 10,
				previewer = false,
			})
		end, "Fuzzy search current buffer")
	end,
}
