local servers = {
	bashls = {},
	clangd = {},
	cmake = {},
	cssls = {},
	docker_compose_language_service = {},
	gopls = {},
	html = {
		filetypes = { "html", "xhtml" }
	},
	-- htmx = {},
	--java_language_server= {},
	jsonls = {},
	lemminx = {},
	lua_ls = {
		Lua = {
			workspace = { checkThirdParty = false },
			telemetry = { enable = false },
		},
	},
	marksman = {},
	omnisharp = {
		cmd = "/usr/lib/omnisharp/OmniSharp.dll",
		enable_editorconfig_support = true,
		enable_ms_build_load_projects_on_demand = false,
		enable_roslyn_analyzers = true,
		organize_imports_on_format = true,
		enable_import_completion = true,
		analyze_open_documents_only = false,
	},
	phpactor = {},
	pkgbuild_language_server = {},
	powershell_es = {},
	pyright = {},
	rust_analyzer = {},
	sqls = {},
	ts_ls = {},
	vala_ls = {},
	vimls = {},
	volar = {},
}

return {
	"neovim/nvim-lspconfig",
	dependencies = {
		"williamboman/mason.nvim",
		"williamboman/mason-lspconfig.nvim",
		"j-hui/fidget.nvim",
		"folke/neodev.nvim",
		"hrsh7th/nvim-cmp",
		"L3MON4D3/LuaSnip",
		"saadparwaiz1/cmp_luasnip",
		"hrsh7th/cmp-nvim-lsp",
		"rafamadriz/friendly-snippets",
	},
	config = function()
		local on_attach = function(_, bufnr)
			KeyMap("n", "<leader>rn", vim.lsp.buf.rename, "LSP: [R]e[n]ame")
			-- KeyMap("n", "<leader>ca", vim.lsp.buf.code_action, "LSP: [C]ode [A]ction")

			KeyMap("n", "gd", vim.lsp.buf.definition, "LSP: [G]oto [D]efinition")
			KeyMap("n", "gD", vim.lsp.buf.declaration, "LSP: [G]oto [D]eclaration")
			KeyMap("n", "gr", require("telescope.builtin").lsp_references, "LSP: [G]oto [R]eferences")
			KeyMap("n", "gI", require("telescope.builtin").lsp_implementations, "LSP: [G]oto [I]mplementation")
			KeyMap("n", "<leader>D", vim.lsp.buf.type_definition, "LSP: Type [D]efinition")
			KeyMap("n", "<leader>ds", require("telescope.builtin").lsp_document_symbols, "LSP: [D]ocument [S]ymbols")
			KeyMap("n", "<leader>ws", require("telescope.builtin").lsp_dynamic_workspace_symbols, "LSP: [W]orkspace [S]ymbols")

			KeyMap("n", "K", vim.lsp.buf.hover, "LSP: Hover Documentation")
			KeyMap("n", "<C-k>", vim.lsp.buf.signature_help, "LSP: Signature Documentation")

			vim.api.nvim_buf_create_user_command(bufnr, "Format", function(_)
				vim.lsp.buf.format()
			end, { desc = "Format current buffer with LSP" })
		end

		local cmp = require("cmp")
		local cmp_lsp = require("cmp_nvim_lsp")
		local capabilities = vim.tbl_deep_extend("force", {}, vim.lsp.protocol.make_client_capabilities(), cmp_lsp.default_capabilities())

		require("fidget").setup({})
		require("mason").setup()
		require("mason-lspconfig").setup({
			ensure_installed = vim.tbl_keys(servers),
			handlers = {
				function(server_name)
					require("lspconfig")[server_name].setup {
						capabilities = capabilities,
						on_attach = on_attach,
						settings = servers[server_name],
						filetypes = (servers[server_name] or {}).filetypes,
					}
				end,
				["lua_ls"] = function()
					local lspconfig = require("lspconfig")
					lspconfig.lua_ls.setup {
						capabilities = capabilities,
						settings = {
							Lua = {
								runtime = { version = "Lua 5.1" },
								diagnostics = {
									globals = { "vim", "it", "describe", "before_each", "after_each" },
								}
							}
						}
					}
				end,
			},
		})
		require("neodev").setup()

		local luasnip = require("luasnip")
		require("luasnip.loaders.from_vscode").lazy_load()
		luasnip.config.setup({})

		cmp.setup({
			snippet = {
				expand = function(args)
					luasnip.lsp_expand(args.body)
				end,
			},
			mapping = cmp.mapping.preset.insert {
				["<C-n>"] = cmp.mapping.select_next_item(),
				["<C-p>"] = cmp.mapping.select_prev_item(),
				["<C-Space>"] = cmp.mapping.complete {},
				["<Tab>"] = cmp.mapping.confirm {
					behavior = cmp.ConfirmBehavior.Replace,
					select = true,
				},
			},
			sources = {
				{ name = "nvim_lsp" },
				{ name = "luasnip" },
				{ name = "buffer" },
			},
		})

		-- vim-dadbod setup
		cmp.setup.filetype({ "sql" }, {
			sources = {
				{ name = "vim-dadbod-completion" },
				{ name = "buffer" },
			},
		})

		-- jsonls
		-- Neovim does not currently include built-in snippets. vscode-json-language-server only provides completions when snippet support is enabled.
		-- To enable completion, install a snippet plugin and add the following override to your language client capabilities during setup.
		-- Enable (broadcasting) snippet capability for completion
		capabilities.textDocument.completion.completionItem.snippetSupport = true
		require("lspconfig").jsonls.setup({
			capabilities = capabilities,
		})
	end,
}
