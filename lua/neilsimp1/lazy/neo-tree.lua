return {
	"nvim-neo-tree/neo-tree.nvim",
	branch = "v3.x",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-tree/nvim-web-devicons",
		"MunifTanjim/nui.nvim",
	},
	config = function()
		require("neo-tree").setup({
			hide_root_node = true,
			buffers = {
				follow_current_file = { enabled = true },
			},
			filesystem = {
				filtered_items = {
					hide_dotfiles = false,
					hide_gitignored = true,
					hide_by_name = {
						".git",
					},
				},
				follow_current_file = { enabled = true },
			},
			sources = {
				"filesystem",
				"buffers",
				"git_status",
				"document_symbols",
			},
			source_selector = {
				statusline = true,
				sources = {
					{ source = "filesystem" },
					{ source = "buffers" },
					{ source = "git_status" },
					{ source = "document_symbols" },
				},
			},
			window = {
				mappings = {
					["t"] = "noop",
				},
			},
		})

		KeyMap("n", "<C-A-e>", function() vim.cmd("Neotree toggle") end)
		KeyMap("n", "<C-e>", function() vim.cmd("Neotree focus") end)
	end,
}
