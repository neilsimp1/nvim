return {
	"folke/noice.nvim",
	event = "VeryLazy",
	dependencies = {
		"MunifTanjim/nui.nvim",
		"rcarriga/nvim-notify",
	},
	config = function ()
		require("noice").setup({
			lsp = {
				override = {
					["vim.lsp.util.convert_input_to_markdown_lines"] = true,
					["vim.lsp.util.stylize_markdown"] = true,
					["cmp.entry.get_documentation"] = true,
				},
			},
			presets = {
				long_message_to_split = true,
				inc_rename = false,
				lsp_doc_border = false,
			}
		})

		KeyMap("n", "<leader>nl", "<Cmd>Noice telescope<CR>", "Noice logs in telescope")
		KeyMap("n", "<leader>nd", "<Cmd>Noice dismiss<CR>", "Noice dismiss")
	end,
}
