return {
	"mbbill/undotree",
	config = function()
		KeyMap("n", "<leader>ut", vim.cmd.UndotreeToggle)
	end
}
