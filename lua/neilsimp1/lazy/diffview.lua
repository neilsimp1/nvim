return {
	"sindrets/diffview.nvim",
	config = function()
		vim.opt.fillchars:append({ diff = "/" })

		local lib = require("diffview.lib")
		local toggle_diffview = function()
			if lib.get_current_view() then
				vim.cmd.DiffviewClose()
			else
				vim.cmd.DiffviewOpen()
			end
		end
		local toggle_diffview_history = function()
			if lib.get_current_view() then
				vim.cmd.DiffviewClose()
			else
				vim.cmd.DiffviewFileHistory("%")
			end
		end
		KeyMap("n", "<leader>dv", toggle_diffview, "Toggle Diffview")
		KeyMap("n", "<leader>dh", toggle_diffview_history, "Diffview File History")

		require("diffview").setup({
			view = {
				merge_tool = {
					layout = "diff3_mixed",
				},
			},
		})
	end,
}
