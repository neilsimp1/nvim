KeyMap("n", "<leader>md", "<Cmd>Glow<CR>")

return {
	"ellisonleao/glow.nvim",
	cmd = "Glow",
	config = function()
		require("glow").setup({
			style = "dark",
			width_ratio = 0.9,
		})
	end,
}
