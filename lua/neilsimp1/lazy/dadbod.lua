return {
	{
		"kristijanhusak/vim-dadbod-ui",
		dependencies = {
			{ "tpope/vim-dadbod", lazy = true },
			{ "kristijanhusak/vim-dadbod-completion", ft = { "sql" }, lazy = true },
		},
		init = function()
			vim.g.db_ui_use_nerd_fonts = 1
			KeyMap("n", "<leader>db", "<Cmd>DBUIToggle<CR>", "DBUI: Toggle UI")
		end,
	},
	{
		"napisani/nvim-dadbod-bg",
		build = "./install.sh",
		config = function()
			vim.cmd([[
				let g:nvim_dadbod_bg_port = "4546"
				let g:nvim_dadbod_bg_log_file = "/tmp/nvim-dadbod-bg.log"
			]])
		end
	}
}
