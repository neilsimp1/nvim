local themes = {
	{
		"comfysage/evergarden",
		opts = { contrast_dark = "hard" },
		config = function()
			vim.cmd.colorscheme("evergarden")
		end,
	},
	{
		"navarasu/onedark.nvim",
		priority = 1000,
		config = function()
			vim.cmd.colorscheme("onedark")
		end,
	},
	{
		"savq/melange-nvim",
		priority = 1000,
		config = function()
			vim.cmd.colorscheme("melange")
		end,
	},
	{
		"ribru17/bamboo.nvim",
		lazy = false,
		priority = 1000,
		config = function()
			require("bamboo").load()
		end,
	},
	{
		"bakageddy/alduin.nvim",
		priority = 1000,
		config = function()
			vim.cmd.colorscheme("alduin")
		end,
	}
}

math.randomseed(os.time())
return themes[math.random(#themes)]
