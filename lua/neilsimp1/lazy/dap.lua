return {
	{
		"mfussenegger/nvim-dap",
		dependencies = {
			{
				"rcarriga/nvim-dap-ui",
				keys = {
					{ "<leader>du", function() require("dapui").toggle({ }) end, desc = "Dap UI" },
					{ "<leader>de", function() require("dapui").eval() end, desc = "Eval", mode = {"n", "v"} },
				},
				dependencies = { "nvim-neotest/nvim-nio" },
				opts = {},
				config = function(_, opts)
					require("dap.ext.vscode").load_launchjs()
					local dap = require("dap")
					local dapui = require("dapui")
					dapui.setup(opts)
					dap.listeners.before.attach.dapui_config = function()
						dapui.open()
						On_dapui_open()
					end
					dap.listeners.before.launch.dapui_config = function()
						dapui.open()
						On_dapui_open()
				end
					dap.listeners.before.event_terminated.dapui_config = function()
					dapui.close()
						On_dapui_close()
					end
					dap.listeners.before.event_exited.dapui_config = function()
						dapui.close()
						On_dapui_close()
					end
				end,
			},
			{
				"theHamsta/nvim-dap-virtual-text",
				opts = {},
			},
			{
				"jay-babu/mason-nvim-dap.nvim",
				dependencies = "mason.nvim",
				cmd = { "DapInstall", "DapUninstall" },
				opts = {
					automatic_installation = true,
					handlers = {},
					ensure_installed = {
						-- "",
					},
				},
			},

			-- language-specific adapters
			"jbyuki/one-small-step-for-vimkind",
			"mfussenegger/nvim-dap-python",
		},

		config = function()
			vim.api.nvim_set_hl(0, "DapStoppedLine", { default = true, link = "Visual" })

			local has_dap, dap = pcall(require, "dap")
			if not has_dap then
				return
			end

			local function get_program_path()
				return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
			end

			local function get_args()
				return vim.split(vim.fn.input("Arguments: "), " ")
			end

			vim.fn.sign_define("DapBreakpoint", { text = "⚫", texthl = "", linehl = "", numhl = "" })
			vim.fn.sign_define("DapBreakpointCondition", { text = "⚪", texthl = "", linehl = "", numhl = "" })
			vim.fn.sign_define("DapStopped", { text = "👉", texthl = "", linehl = "DapStoppedLine", numhl = "" })

			require("nvim-dap-virtual-text").setup({
				enabled = true,
				enabled_commands = false,
				highlight_changed_variables = true,
				highlight_new_as_changed = true,
				commented = false,
				show_stop_reason = true,
			})

			-- C/C++/Rust
			dap.adapters.lldb = {
				type = "executable",
				command = "/usr/bin/lldb-vscode",
				name = "lldb",
			}

			dap.configurations.c = {
				{
					name = "Launch",
					type = "lldb",
					request = "launch",
					program = get_program_path,
					cwd = "${workspaceFolder}",
					stopOnEntry = false,
					args = {},
					runInTerminal = false,
					console = "integratedTerminal",
				},
				{
					name = "Launch with arguments",
					type = "lldb",
					request = "launch",
					program = get_program_path,
					cwd = "${workspaceFolder}",
					stopOnEntry = false,
					args = get_args,
					runInTerminal = false,
					console = "integratedTerminal",
				},
			}

			dap.configurations.cpp = dap.configurations.c
			dap.configurations.rust = dap.configurations.c

			-- C#
			dap.adapters.coreclr = {
				type = "executable",
				command = "/usr/bin/netcoredbg",
				args = { "--interpreter=vscode" }
			}

			dap.configurations.cs = {
				{
					type = "coreclr",
					name = "Launch",
					request = "launch",
					program = get_program_path,
					console = "integratedTerminal",
				},
			}

			-- JavaScript
			dap.adapters.node2 = {
				type = "executable",
				command = "node",
				args = { "/usr/lib/vscode-node-debug2/out/src/nodeDebug.js" },
			}

			dap.configurations.javascript = {
				{
					name = "Launch",
					type = "node2",
					request = "launch",
					program = "${file}",
					cwd = vim.fn.getcwd(),
					sourceMaps = true,
					protocol = "inspector",
					console = "integratedTerminal",
				},
				{
					name = "Launch with arguments",
					type = "node2",
					request = "launch",
					program = "${file}",
					cwd = vim.fn.getcwd(),
					sourceMaps = true,
					protocol = "inspector",
					console = "integratedTerminal",
					args = get_args,
				},
				{
					name = "Attach to process",
					type = "node2",
					request = "attach",
					processId = require("dap.utils").pick_process,
				},
			}

			-- Lua
			dap.adapters["local-lua"] = {
				type = "executable",
				command = "local-lua-dbg",
				enrich_config = function(config, on_config)
					if not config["extensionPath"] then
						local c = vim.deepcopy(config)
						c.extensionPath = "/usr/lib/node_modules/local-lua-debugger-vscode/"
						on_config(c)
					else
						on_config(config)
					end
				end,
			}

			dap.adapters.nlua = function(callback, config)
				callback({ type = "server", host = config.host, port = config.port })
			end

			dap.configurations.lua = {
				{
					name = "Launch file",
					type = "local-lua",
					request = "launch",
					cwd = "${workspaceFolder}",
					program = {
						lua = "lua",
						file = "${file}",
					},
					verbose = true,
					args = {},
					console = "integratedTerminal",
				},
				{
					type = "nlua",
					request = "attach",
					name = "Attach to running Neovim instance",
					host = function() return "127.0.0.1" end,
					port = function() return 54321 end,
				},
			}

			-- PHP
			dap.adapters.php = {
				type = "executable",
				command = "node",
				args = { "/usr/lib/node_modules/php-debug/out/phpDebug.js" }
			}

			dap.configurations.php = {
				{
					type = "php",
					request = "launch",
					name = "Listen for Xdebug",
				}
			}

			-- Python
			local function get_python_path()
				local cwd = vim.fn.getcwd()
				if vim.fn.executable(cwd .. "/venv/bin/python") == 1 then
					return cwd .. "/venv/bin/python"
				elseif vim.fn.executable(cwd .. "/.venv/bin/python") == 1 then
					return cwd .. "/.venv/bin/python"
				else
					return "/usr/bin/python"
				end
			end

			dap.configurations.python = {
				{
					type = "python",
					request = "launch",
					name = "Launch file",
					program = "${file}",
					pythonPath = get_python_path,
					console = "integratedTerminal",
				},
				{
					type = "python",
					request = "launch",
					name = "Launch file with arguments",
					program = "${file}",
					pythonPath = get_python_path,
					args = get_args,
					console = "integratedTerminal",
				},
			}

			local dap_python = require("dap-python")
			dap_python.setup("python", {
				console = "integratedTerminal",
				include_configs = true,
			})
			dap_python.test_runner = "pytest"

			KeyMap("n", "<F5>", require("dap").continue, "DAP: Continue")
			KeyMap("n", "<S-F5>", require("dap").terminate, "DAP: Terminate")
			KeyMap("n", "<F7>", require("dapui").eval)
			KeyMap("n", "<S-F7>", function()
				require("dapui").eval(vim.fn.input "DAP: Expression > ")
			end)
			KeyMap("n", "<F8>", require("dap").repl.open)
			KeyMap("n", "<F9>", require("dap").toggle_breakpoint)
			KeyMap("n", "<S-F9>", function()
				require("dap").set_breakpoint(vim.fn.input "DAP: Condition > ")
			end)
			KeyMap("n", "<F10>", require("dap").step_over, "DAP: Step Over")
			KeyMap("n", "<F11>", require("dap").step_into, "DAP: Step Into")
			KeyMap("n", "<S-F11>", require("dap").step_out, "DAP: Step Out")
			KeyMap("n", "<F12>", require("dap").step_back, "DAP: Step Back")
		end,
	},

	{
		"linux-cultist/venv-selector.nvim",
		dependencies = {
			"neovim/nvim-lspconfig",
			"mfussenegger/nvim-dap", "mfussenegger/nvim-dap-python", --optional
			{ "nvim-telescope/telescope.nvim", branch = "0.1.x", dependencies = { "nvim-lua/plenary.nvim" } },
		},
		lazy = false,
		branch = "regexp", -- This is the regexp branch, use this for the new version
		config = function()
			require("venv-selector").setup({
				settings = {
					search = {
						my_venvs = {
							command = "fd python$ ~/.local/share/virtualenvs/",
						},
					},
				},
			})
		end,
		keys = {
			{ "<leader>ve", "<cmd>VenvSelect<cr>" },
		},
	}
}
