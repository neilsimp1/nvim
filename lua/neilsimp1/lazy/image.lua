return {
	{
		"vhyrro/luarocks.nvim",
		priority = 1001,
		config = true,
	},
	{
		"3rd/image.nvim",
		dependencies = { "luarocks.nvim" },
		opts = {},
	},
}
