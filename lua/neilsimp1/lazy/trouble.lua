return {
	"folke/trouble.nvim",
	dependencies = { "nvim-tree/nvim-web-devicons" },
	config = function()
		local trouble = require("trouble")
		KeyMap("n", "<C-A-d>", function() trouble.toggle(TroubleMode) end)
		KeyMap("n", "<C-d>", function() trouble.open(TroubleMode) end)
		trouble.setup({
			focus = true,
			open_no_results = true,
			warn_no_results = false,
		})
	end,
}
