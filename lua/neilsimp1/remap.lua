vim.g.mapleader = " "
KeyMap({ "n", "v" }, "<Space>", "<Nop>")

KeyMap("n", "<C-S-h>", function() vim.cmd("wincmd h") end)
KeyMap("n", "<C-S-j>", function() vim.cmd("wincmd j") end)
KeyMap("n", "<C-S-k>", function() vim.cmd("wincmd k") end)
KeyMap("n", "<C-S-l>", function() vim.cmd("wincmd l") end)

KeyMap("n", "<C-t>", function() vim.cmd("enew") end)

KeyMap("v", "<Tab>", ">gv")
KeyMap("v", "<S-Tab>", "<gv")

KeyMap("v", "<C-c>", '"+y')

KeyMap({"n", "v"}, "<Home>", "^")
KeyMap("i", "<Home>", "<C-o>^")
