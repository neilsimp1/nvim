local function split(inputstr, sep)
	if sep == nil then
		sep = "%s"
	end
	local t = {}
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		table.insert(t, str)
	end
	return t
end

local function sub_hex_ent(s) return string.char(tonumber(s, 16)) end
local function sub_dec_ent(s) return string.char(tonumber(s) or 0) end

XmlEscape = function()
	local xml = table.concat(vim.api.nvim_buf_get_lines(0, 0, -1, false), "\n")
	xml = string.gsub(xml, "&", "&amp;")
	xml = string.gsub(xml, "<", "&lt;")
	xml = string.gsub(xml, ">", "&gt;")
	xml = string.gsub(xml, "'", "&apos;")
	xml = string.gsub(xml, '"', "&quot;")
	vim.api.nvim_buf_set_lines(0, 0, -1, false, split(xml, "\n"))
end

XmlUnescape = function()
	local xml = table.concat(vim.api.nvim_buf_get_lines(0, 0, -1, false), "\n")
	xml = string.gsub(xml, "&lt;", "<")
	xml = string.gsub(xml, "&gt;", ">")
	xml = string.gsub(xml, "&apos;", "'")
	xml = string.gsub(xml, "&quot;", '"')
	xml = string.gsub(xml, "&#x(%x+);", sub_hex_ent)
	xml = string.gsub(xml, "&#(%d+);", sub_dec_ent)
	xml = string.gsub(xml, "&amp;", "&")
	vim.api.nvim_buf_set_lines(0, 0, -1, false, split(xml, "\n"))
end


XmlMaxTab = function()
	vim.cmd("XmlMax")
	vim.cmd("%s/	/\t/g")
	vim.cmd("nohlsearch|diffupdate|normal! <C-L>")
end

local commands = {
	{
		name = "EscapeQuotes",
		exec = '%s/"/\\"/g',
		desc = 'Escape " to \\"',
	},
	{
		name = "UnescapeQuotes",
		exec = '%s/\\"/"/g',
		desc = 'Escape \\" to "',
	},
	{
		name = "JsonMax",
		exec = "%!jq .",
		desc = "JSON Prettify",
	},
	{
		name = "JsonMin",
		exec = "%!jq -c",
		desc = "JSON Minify",
	},
	{
		name = "XmlEscape",
		exec = "lua XmlEscape()",
		desc = "Escape XML",
	},
	{
		name = "XmlUnescape",
		exec = "lua XmlUnescape()",
		desc = "Unescape XML",
	},
	{
		name = "XmlMax",
		exec = '%!xmllint --format "%"',
		desc = "XML Prettify",
	},
	{
		name = "XmlMaxTab",
		exec = "lua XmlMaxTab()",
		desc = "XML Prettify (Use Tabs)",
	},
	{
		name = "XmlMin",
		exec = '%!xmllint --noblanks "%"',
		desc = "XML Minify",
	}
}

for _, command in ipairs(commands) do
	vim.api.nvim_create_user_command(command.name, command.exec, { desc = command.desc })
end
