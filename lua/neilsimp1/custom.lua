local function cycle(list, current_tab, dir)
	local next_index = 1
	for i = 1, #list do
		if list[i] == current_tab then
			if dir == 0 then
				next_index = i == 1 and #list or i - 1
			else
				next_index = i == #list and 1 or i + 1
			end
		end
	end
	return list[next_index]
end

local function cycle_neotree_source(dir)
	NeoTreeSource = cycle(NeoTreeSources, NeoTreeSource, dir)
	return "Neotree "..NeoTreeSource
end

local function cycle_trouble_mode(dir)
	vim.cmd("Trouble toggle "..TroubleMode)
	TroubleMode = cycle(TroubleModes, TroubleMode, dir)
	return "Trouble "..TroubleMode
end

local function get_current_ft()
	return vim.api.nvim_get_option_value("filetype", {  buf = vim.api.nvim_get_current_buf() })
end

local function change_tab(dir)
	local current_ft = get_current_ft()

	if current_ft == "neo-tree" then
		return cycle_neotree_source(dir)
	elseif current_ft == "trouble" then
		return cycle_trouble_mode(dir)
	else
		return dir == 0 and "BufferLineCyclePrev" or "BufferLineCycleNext"
	end
end

KeyMap("n", "<C-S-Tab>", function() vim.cmd(change_tab(0)) end)
KeyMap("n", "<C-Tab>", function() vim.cmd(change_tab(1)) end)
KeyMap("n", "<C-b>", function()
	vim.cmd("wincmd k")
	vim.cmd("wincmd l")
end)

function On_dapui_open()
	vim.cmd("Neotree close")
end
function On_dapui_close()
	vim.cmd("Neotree show")
end

-- Duplicate a line and comment out the first line
KeyMap("n", "yc", "yygccp", "Duplicate and comment out line", true, true)
