#!/bin/sh

if [ "$(pwd)" != "$HOME/.config/nvim" ]; then
	rm -rf ~/.config/nvim
	ln -s "$(pwd)" ~/.config/nvim
fi

rm ~/.config/kitty/kitty.conf
ln -s "$(pwd)"/kitty.conf ~/.config/kitty/kitty.conf

rm ~/.local/share/applications/nvim.desktop
ln -s "$(pwd)"/nvim.desktop ~/.local/share/applications/nvim.desktop
