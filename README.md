# Deploy
`./deploy.sh` will do the following:
1. Delete and symlink this directory to `~/.config/nvim/`, if that is not the current directory.
3. Delete and symlink kitty.conf to `~/.config/kitty/kitty.conf`.
4. Delete and symlink nvim.desktop to `~/.local/share/applications`.

# Dependencies
- debugpy
- lazygit
- lldb
- libxml2
- local-lua-debugger-vscode
- jq
- netcoredbg
- ripgrep
- vscode-node-debug2
- vscode-php-debug
- wl-clipboard
- xdebug
